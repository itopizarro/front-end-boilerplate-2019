// modules/bar.js
// A placeholder example of a custom module

class Bar {
    constructor() {
        this.foo = false;
    }
    init() {
        console.log( 'THIS IS THE EXAMPLE MODULE', this.foo );
    }
}

export default Bar;