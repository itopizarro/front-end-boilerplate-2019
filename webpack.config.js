const 
    project_configuration = require("./project.config"),
    path = require("path"),
    webpack = require("webpack"),
    CopyPlugin = require("copy-webpack-plugin"),
    MiniCssExtractPlugin = require("mini-css-extract-plugin");

let webpack_configuration = {
  entry: [
    "@babel/polyfill",
    project_configuration.theme.path +
      project_configuration.source.path +
      project_configuration.source.scripts.path +
      project_configuration.source.scripts.build.entry,
    project_configuration.theme.path +
      project_configuration.source.path +
      project_configuration.source.sass.path +
      "main.scss",
    project_configuration.theme.path +
      project_configuration.source.path +
      project_configuration.source.sass.path +
      "chrome.scss"
  ],

  output: {
    filename: "main.build.js",
    path: path.resolve(
      __dirname,
      project_configuration.theme.path +
        project_configuration.asset.path
    ) // NOTE: All the paths to the various other assets ORIGINATE here
  },
  resolve: {
    alias: {
      jquery: "jquery/dist/jquery.js",
      _: "lodash",
      "/node_modules": path.resolve(__dirname, "./node_modules")
    },
    modules: [
      path.resolve(
        __dirname,
        project_configuration.theme.path +
          project_configuration.source.path +
          project_configuration.source.scripts.path +
          "components"
      ),
      path.resolve(
        __dirname,
        project_configuration.theme.path +
          project_configuration.source.path +
          project_configuration.source.scripts.path +
          "models"
      ),
      path.resolve(
        __dirname,
        project_configuration.theme.path +
          project_configuration.source.path +
          project_configuration.source.scripts.path +
          "constants"
      ),
      path.resolve(
        __dirname,
        project_configuration.theme.path +
          project_configuration.source.path +
          project_configuration.source.scripts.path +
          "modules"
      ),
      path.resolve(
        __dirname,
        project_configuration.theme.path +
          project_configuration.source.path +
          project_configuration.source.scripts.path +
          "vendor"
      ),
      path.resolve(__dirname, "node_modules")
    ]
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: "style-loader"
          },
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: "css-loader"
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/,
        use: [
          {
            loader: "file-loader",
            options: {
            //   name: "../images/[name].[ext]"
              name: ( () => {
                    let
                        asset_path =  project_configuration.asset.images.path,
                        string = '[name].[ext]';
                    if ( typeof asset_path !== 'string' ) { return string; }
                    if ( asset_path.length ) {
                        return `${ asset_path }/${ string }`;
                    }
                    return string;
                } )()
            }
          }
        ]
      },
      {
        test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              // publicPath: '/ui/dist/fonts/',
            //   outputPath: "../fart/"
              outputPath: ( () => {
                let asset_path = project_configuration.asset.fonts.path;

                if ( typeof asset_path != 'string' ) { return; }
                if ( asset_path.length ) {
                    return `${ asset_path }`;
                }
                return;
            } )()
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery",
      _: "_"
    }),
    new CopyPlugin([
      {
        from:
          project_configuration.theme.path +
          project_configuration.source.path +
          project_configuration.source.images.path,
        to: project_configuration.asset.images.path
      }
    ]),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      filename: ( () => {
          let
            asset_path =  project_configuration.asset.styles.path,
            string = '[name].build.css';
          if ( typeof asset_path !== 'string' ) { return string; }
          if ( asset_path.length ) {
              return `${ asset_path }/${ string }`;
          }
          return string;
      } )()
    }) 
  ]
};

module.exports = (environment, argument_variables) => {
  // settings for development mode
  if (argument_variables.mode === "development") {
    webpack_configuration.devtool = "source-map";
    webpack_configuration.watch = true;
  }

  // settings for production mode
  /*
    if (argument_variables.mode === "production") {
        // stuff
    }
    */

  return webpack_configuration;
};
